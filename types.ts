export type Command = {
  type: 'SignUpUser'
  username: string
  password: string
}

export type AppEvent = {
  type: 'UserCreated'
  username: string
  password: string
  user_id: number
}

export type Query = {
  type: 'UserList'
  page: number
  count: number
}

export type QueryResponse = {
  userList: Array<{
    username: string
  }>
}
