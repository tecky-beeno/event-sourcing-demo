import express from 'express'
import { ViewModel } from './view-model'
import { WriteModel } from './write-model'

export class Engine {
  constructor(public writeModel: WriteModel, public viewModel: ViewModel) {}

  attach(app: express.Router) {
    app.post('/command', express.json(), (req, res) => {
      try {
        let events = this.writeModel.handleCommand(req.body)
        events.forEach(event => this.viewModel.onEvent(event))
        res.json({})
      } catch (error: any) {
        res.json({ error: error.toString() })
      }
    })

    app.get('/query', express.urlencoded({ extended: true }), (req, res) => {
      try {
        let response = this.viewModel.handleQuery(req.query as any)
        res.json(response)
      } catch (error: any) {
        res.json({ error: error.toString() })
      }
    })
  }
}
