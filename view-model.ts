import { AppEvent, Query, QueryResponse } from './types'

export class ViewModel {
  userList: Array<{ username: string; password: string; user_id: number }> = []

  onEvent(event: AppEvent) {
    this.userList.push(event)
  }

  getUserList() {
    return {
      userList: this.userList.map(user => ({ username: user.username })),
    }
  }

  handleQuery(query: Query): QueryResponse {
    return this.getUserList()
  }
}
