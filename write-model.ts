import { AppEvent, Command } from './types'

export class WriteModel {
  user_count = 0

  handleCommand(command: Command): AppEvent[] {
    this.user_count++
    return [
      {
        type: 'UserCreated',
        username: command.username,
        password: command.password,
        user_id: this.user_count,
      },
    ]
  }
}
