import express from 'express'
import { Engine } from './engine'
import { ViewModel } from './view-model'
import { WriteModel } from './write-model'

let viewModel = new ViewModel()
let writeModel = new WriteModel()

let engine = new Engine(writeModel, viewModel)

let app = express()

engine.attach(app)

app.listen(3000)
