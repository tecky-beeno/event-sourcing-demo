import { Command } from './types'

export function submitCommand(command: Command) {
  return fetch('/command', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(command),
  }).then(res => res.json())
}
