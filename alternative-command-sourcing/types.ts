interface AttemptUserSignupCommand {
  email: string
  password: string
}

interface AuthUserSignupCommand {
  email: string
  password: string
}

interface UserSignupEvent {
  email: string
  password: string
}

interface StartNewGameCommand {
  lower: number
  upper: number
}

interface CreateNewGameCommand {
  lower: number
  upper: number
  answer
}
